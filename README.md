# Juggling

Juggling is a fun hobby with many benefits including:
- Stress-reliever
- Hand-eye coordination
- Strengthens both white and grey brain matter
- Focus/concentration
- Self-confidence
- Improves Persistence/ resilience
- Workout (280 calories/hour)
- Fun!

Juggling is the ability to manipulate more objects than one has hands for. Most commonly, a person with two hands throws and catches three balls. There are seemingly an infinite number of tricks and patterns to learn.

## Why juggle?
- [Here’s What Learning to Juggle Does to Your Brain](https://www.wired.com/story/heres-what-learning-juggle-does-your-brain/). Excellent article.


## How-to

Practicing for 5-10 minutes a day will greatly improve your skills.

- [Learn to JUGGLE 3 BALLS - Beginner Tutorial](https://www.youtube.com/watch?v=dCYDZDlcO6g). Excellent beginning juggling tutorial by Taylor Tries.
- [IJA Tutorials](https://www.juggle.org/category/ejuggle-tutorials/). Tutorials of varous juggling skills.
- [5 Easy JUGGLING TRICKS - Beginner Tutorial](https://www.youtube.com/watch?v=rvxfXEHML4s). Great progression of beginner tricks by Taylor Tries.

## Juggling props/vendors

- [Renegade Juggling](https://www.renegadejuggling.com/). US-based.
- [Dube](https://www.dube.com/). US-based.
- [Higgins Bros](https://higginsbrothers.com/en/). Canada-based
- [Play Juggling](https://www.playjuggling.com/en/). Italy-based. Excellent clubs manufacturer.
- [K8 Malabares](https://k8malabares.com/). Argentina-based.
- [Three Worlds](https://www.threeworlds.com.au/collections/juggling-balls). Australia-based.
- [gBallz](https://gballz.com/collections). US-based handmade (expensive) juggling beanbags.

## Juggling organizations/events

- [International Jugglers' Association (IJA)](https://docs.gitlab.com/ee/user/project/members/). North-America-based.  Hosts a large annual week-long juggling festival every July.
- [Europeaan Juggling Convention (EJC)](https://www.ejc2024.org/).  Europe-based.  Hosts a large annual week-long juggling festival every July/August.  Larger than the IJA.
- [Juggling Edge](hhttps://www.jugglingedge.com/events.php). List of many regional juggling festivals.
